/*//GET 
async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/todos');
	console.log(result);

	console.log(typeof result);

	let json = await result.json();

	console.log(json);
}

fetchData();


// GET


//POST


// PUT
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated post',
		body: 'Hello again!',
		userId: 1
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));

// UPDATE


// PATCH

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		title: 'Corrected Post'
	}),
})
.then((response) => response.json())
.then((json) => console.log(json)); */


// array
fetch('https://jsonplaceholder.typicode.com/todos')
    .then(response => response.json())
    .then(json => {
        json.map((item) => {
        console.log(item.title) 
  })
})

//

fetch('https://jsonplaceholder.typicode.com/todos/1')
        .then(function (res) {
          return res.json();
        })
        .then(function (data) {
          console.log(data)
        })
        .catch(function (err) {
          console.error(err);
        });

console.log('The item "delectus aut autem" on the list has a status of false')

// POST

const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ title: 'Fetch POST Request Example’,
userId: 1
 })
};

fetch('https://jsonplaceholder.typicode.com/todos’, requestOptions)
    .then(response => response.json())
    .then(data => console.log(data );